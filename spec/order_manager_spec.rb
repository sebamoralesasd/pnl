# frozen_string_literal: true

require_relative '../lib/order_manager'
require 'pry'

RSpec.describe OrderManager do
  describe '#execute' do
    subject(:manager) { described_class.new(order_list: [], market_price: market_price) }

    let(:order) { Order.new(time: 1, symbol: 'USDP', price: price, side: side, quantity: quantity) }
    let(:quantity) { 50 }
    let(:price) { 100 }
    let(:market_price) { 99 }

    context 'when no sell was submitted' do
      let(:side) { 'sell' }

      it 'pushes the sell order' do
        expect(manager.sells.count).to eq(0)
        manager.execute(order)
        expect(manager.sells.count).to eq(1)
      end

      it 'returns zero realized pnl' do
        manager.execute(order)
        expect(manager.realized_pnl).to eq(0)
      end
    end

    context 'when no buy was submitted' do
      let(:side) { 'buy' }

      it 'pushes the buy order' do
        expect(manager.buys.count).to eq(0)
        manager.execute(order)
        expect(manager.buys.count).to eq(1)
      end

      it 'returns zero realized pnl' do
        manager.execute(order)
        expect(manager.realized_pnl).to eq(0)
      end
    end

    context 'when the BUY quantity is greater than the sell from queue' do
      let(:side) { 'buy' }
      let(:sell_quantity) { 25 }
      let(:sell_price) { 75 }

      before do
        manager.execute(Order.new(time: 1, symbol: 'USDP', price: sell_price, side: 'sell', quantity: sell_quantity))
      end

      it 'matches and closes the sell' do
        expect(manager.sells.count).to eq(1)
        manager.execute(order)
        expect(manager.sells.count).to eq(0)
      end

      it 'pushes the buy order with the updated quantity' do
        expect(manager.buys.count).to eq(0)
        manager.execute(order)
        expect(manager.buys.count).to eq(1)
        expect(manager.buys.first.quantity).to eq(quantity - sell_quantity)
      end

      it 'calculates the realized pnl' do
        expect(manager.realized_pnl).to eq(0)
        manager.execute(order)
        expect(manager.realized_pnl).to eq (price - sell_price) * -sell_quantity
      end

      it 'calculates the unrealized pnl' do
        manager.execute(order)
        expect(manager.unrealized_pnl).to eq(-25)
      end
    end

    context 'when the BUY quantity is lower than the sell from queue' do
      let(:side) { 'buy' }
      let(:sell_quantity) { 100 }
      let(:sell_price) { 75 }

      before do
        manager.execute(Order.new(time: 1, symbol: 'USDP', price: sell_price, side: 'sell', quantity: sell_quantity))
      end

      it 'matches but not closes the sell' do
        expect(manager.sells.count).to eq(1)
        manager.execute(order)
        expect(manager.sells.count).to eq(1)
      end

      it 'does not push the buy order' do
        expect(manager.buys.count).to eq(0)
        manager.execute(order)
        expect(manager.buys.count).to eq(0)
      end

      it 'calculates the realized pnl' do
        expect(manager.realized_pnl).to eq(0)
        manager.execute(order)
        expect(manager.realized_pnl).to eq (price - sell_price) * -quantity
      end

      it 'calculates the unrealized pnl' do
        manager.execute(order)
        expect(manager.unrealized_pnl).to eq(1200)
      end
    end

    context 'when the SIDE quantity is greater than the buy from queue' do
      let(:side) { 'sell' }
      let(:buy_quantity) { 25 }
      let(:buy_price) { 75 }

      before do
        manager.execute(Order.new(time: 1, symbol: 'USDP', price: buy_price, side: 'buy', quantity: buy_quantity))
      end

      it 'matches and closes the buy' do
        expect(manager.buys.count).to eq(1)
        manager.execute(order)
        expect(manager.buys.count).to eq(0)
      end

      it 'pushes the sell order with the updated quantity' do
        expect(manager.sells.count).to eq(0)
        manager.execute(order)
        expect(manager.sells.count).to eq(1)
        expect(manager.sells.first.quantity).to eq(quantity - buy_quantity)
      end

      it 'calculates the realized pnl' do
        expect(manager.realized_pnl).to eq(0)
        manager.execute(order)
        expect(manager.realized_pnl).to eq (price - buy_price) * buy_quantity
      end

      it 'calculates the unrealized pnl' do
        manager.execute(order)
        expect(manager.unrealized_pnl).to eq(-25)
      end
    end

    context 'when the sell quantity is lower than the buy from queue' do
      let(:side) { 'sell' }
      let(:buy_quantity) { 100 }
      let(:buy_price) { 75 }

      before do
        manager.execute(Order.new(time: 1, symbol: 'USDP', price: buy_price, side: 'buy', quantity: buy_quantity))
      end

      it 'matches but not closes the buy' do
        expect(manager.buys.count).to eq(1)
        manager.execute(order)
        expect(manager.buys.count).to eq(1)
      end

      it 'does not push the sell order' do
        expect(manager.sells.count).to eq(0)
        manager.execute(order)
        expect(manager.sells.count).to eq(0)
      end

      it 'calculates the realized pnl' do
        expect(manager.realized_pnl).to eq(0)
        manager.execute(order)
        expect(manager.realized_pnl).to eq (price - buy_price) * quantity
      end

      it 'calculates the unrealized pnl' do
        manager.execute(order)
        expect(manager.unrealized_pnl).to eq(1200)
      end
    end
  end
end
