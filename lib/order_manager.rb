# frozen_string_literal: true

require_relative 'order'

# Consume compras y ventas abiertas y calcula el PNL realizado a partir de un order externo.
class OrderManager
  attr_accessor :buys, :sells
  attr_reader :realized_pnl, :unrealized_pnl, :net_pnl

  def initialize(params)
    @order_list = params[:order_list]
    @market_price = params[:market_price]
    @buys = []
    @sells = []
    @closed_orders = []
    @realized_pnl = 0
    @unrealized_pnl = 0
    @net_pnl = 0
    @average_price = 0
  end

  def execute_list
    @order_list.each { |t| execute(t) }
    average_price
  end

  def execute(order)
    return unless open_orders.empty? || symbol_match(order, open_orders.first)

    if order.side == 'buy'
      close_sells(order)
    else # order.side == 'S'
      close_buys(order)
    end
    # calcular pnl neto
    set_net_pnl
  end

  def print
    puts "Compras abiertas: #{@buys.count}, Ventas abiertas: #{@sells.count}, PNL realizado: #{@realized_pnl} " \
         "PNL NO realizado: #{@unrealized_pnl}, PNL neto: #{@net_pnl}, Precio promedio: #{@average_price}, Precio market: #{@market_price}"
  end

  def print_closed_orders
    puts 'orders cerrados'
    @closed_orders.map(&:print)
  end

  private

  def set_net_pnl
    set_unrealized_pnl
    @net_pnl = @realized_pnl + @unrealized_pnl
  end

  def close_sells(buy)
    if @sells.empty?
      @buys.push(buy)
    else
      # matchear todos los sells posibles
      buy = match_sells(buy)
      @buys.push(buy) if @sells.empty? && buy.quantity.positive?
    end
  end

  def close_buys(sell)
    if @buys.empty?
      @sells.push(sell)
    else
      # matchear todos los buys posibles
      sell = match_buys(sell)
      @sells.push(sell) if @buys.empty? && sell.quantity.positive?
    end
  end

  def symbol_match(order_a, order_b)
    order_a.symbol == order_b.symbol
  end

  def match_sells(buy)
    #puts "matcheando sells con buy #{buy.print}"
    return buy if buy.quantity.zero?

    sell = @sells.first
    #puts "matcheando sell #{sell.print}"
    # menos cant de fills de buys que fills de la venta a matchear
    if sell.quantity > buy.quantity
      pnl = (buy.price - sell.price) * -buy.quantity
      submit_closed_order(sell, buy, pnl)
      @realized_pnl += pnl
      sell.quantity -= buy.quantity
      return buy

    else # misma cant o mas fills de buys que fills de la venta a matchear
      pnl = (buy.price - sell.price) * -sell.quantity
      submit_closed_order(sell, buy, pnl)
      @realized_pnl += pnl
      buy.quantity -= sell.quantity
      # se elimina el sell cerrado
      @sells.shift
    end

    return buy if @sells.empty?

    match_sells(buy)
  end

  def match_buys(sell)
    #puts "matcheando buys con sell #{sell.print}"
    # puts "cantidad sell: #{sell.quantity}"
    return sell if sell.quantity.zero?

    buy = @buys.first
    #puts "matcheando buy #{buy.print}"
    # menos cant de fills de sells que fills de la compra a matchear
    if buy.quantity > sell.quantity
      pnl = (sell.price - buy.price) * sell.quantity
      submit_closed_order(buy, sell, pnl)
      @realized_pnl += pnl
      buy.quantity -= sell.quantity
      return sell

    else # misma cant o mas fills de sells que fills de la compra a matchear
      pnl = (sell.price - buy.price) * buy.quantity
      submit_closed_order(buy, sell, pnl)
      @realized_pnl += pnl
      sell.quantity -= buy.quantity
      # se elimina el buy cerrado
      @buys.shift
    end

    return sell if @buys.empty?

    match_buys(sell)
  end

  # Precio promedio sobre open orders finales
  def average_price
    final_holdings = open_orders.map(&:extension).sum
    @average_price = final_holdings / quantity_open_orders
  end

  def set_unrealized_pnl
    value = @market_price * quantity_open_orders
    final_holdings = open_orders.map(&:extension).sum
    # puts "#{value}, #{final_holdings}"

    @unrealized_pnl = value - final_holdings
  end

  def open_orders
    @buys.count.zero? ? @sells : @buys
  end

  def quantity_open_orders
    qty = 0
    open_orders.each { |ct| qty += ct.quantity }
    qty
  end

  def submit_closed_order(open_order, closer_order, pnl)
    params = {
      open_time: open_order.time,
      close_time: closer_order.time,
      symbol: open_order.symbol,
      quantity: [open_order.quantity, closer_order.quantity].min,
      pnl: pnl,
      open_side: open_order.side,
      close_side: closer_order.side,
      open_price: open_order.price,
      close_price: closer_order.price
    }

    closed_order = ClosedOrder.new(params)
    @closed_orders << closed_order
  end
end
