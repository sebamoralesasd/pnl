# frozen_string_literal: true

require_relative 'order'
require_relative 'order_manager'
require_relative 'concrete_creator'

def print_order_list(order_list)
  filter_order_list = order_list.select { |tr| tr.symbol == order_list.first.symbol }

  filter_order_list.map(&:print)
end

def ejemplo_stackexchange
  stack_exch = [
    Order.new(time: 1, symbol: 'MSFT', side: 'B', price: 100, quantity: 12),
    Order.new(time: 2, symbol: 'MSFT', side: 'B', price: 99, quantity: 17),
    Order.new(time: 3, symbol: 'MSFT', side: 'B', price: 103, quantity: 3),
    Order.new(time: 4, symbol: 'MSFT', side: 'S', price: 101, quantity: 9),
    Order.new(time: 5, symbol: 'MSFT', side: 'S', price: 105, quantity: 4)
  ]

  puts 'Ejemplo stackexchange'
  puts print_order_list(stack_exch)
  manager = OrderManager.new(order_list: stack_exch, market_price: 99)
  manager.execute_list
  manager.print
  puts
  manager.print_closed_orders
end

def ejemplo_activestate
  active_state = [
    Order.new(time: 1, symbol: 'MSFT', side: 'B', price: 25.1, quantity: 75),
    Order.new(time: 1, symbol: 'MSFT', side: 'B', price: 25.12, quantity: 50),
    Order.new(time: 1, symbol: 'MSFT', side: 'S', price: 25.22, quantity: 100)
  ]

  puts 'Ejemplo activestate'
  puts print_order_list(active_state)
  manager = OrderManager.new(order_list: active_state, market_price: 25)
  manager.execute_list
  manager.print
  manager.print_closed_orders
end

def ejemplo_csv
  raise 'Falta CSV' if ARGV.length != 1

  csv_file = "./#{ARGV.first}"

  runner = ConcreteCreator.new(file: csv_file)

  order_list = runner.factory_method
  puts print_order_list(order_list)
  manager = OrderManager.new(order_list: order_list, market_price: 1)

  manager.execute_list
  puts
  manager.print
  puts
  #manager.print_closed_orders
end

def main
  # Ejemplo stackexchange
  # ejemplo_stackexchange
  # Ejemplo activestate
  # ejemplo_activestate

  ejemplo_csv
end

main
