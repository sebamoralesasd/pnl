# frozen_string_literal: true

require 'csv'

require_relative 'order'
require_relative 'creator'

class ConcreteCreator < Creator
  def initialize(params)
    @file = CSV.read(params[:file]).drop(1) unless params[:file] == ''
  end

  def factory_method
    order_list = []

    @file.each do |row|
      order_list << create_order(row)
    end

    order_list
  end

  def create_order(row)
    time = row[0].to_i
    price = row[1].to_f
    quantity = row[2].to_f
    side = row[-2]
    symbol = row[6]

    Order.new(time: time, symbol: symbol, side: side, price: price, quantity: quantity)
  end
end

# def main
#   raise 'Falta CSV' if ARGV.length != 1

#   csv_file = "./#{ARGV.first}"

#   runner = ConcreteCreator.new(file: csv_file)

#   puts(runner.factory_method)
# end

# main
