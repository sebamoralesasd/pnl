# frozen_string_literal: true

# Order submitteado.
class Order
  attr_accessor :time, :symbol, :side, :price, :quantity

  # Time, Symbol, Side, Price, Quantity
  def initialize(params)
    @time = params[:time]
    @symbol = params[:symbol]
    @side = params[:side]
    @price = params[:price]
    @quantity = params[:quantity]
  end

  def print
    "TIME: #{@time}, SYMBOL: #{@symbol}, SIDE: #{@side}, PRICE: #{@price}, QTY: #{@quantity}."
  end

  def extension
    @price * @quantity
  end
end

# Order matcheado.
class ClosedOrder
  attr_reader :quantity

  def initialize(params)
    @open_time = params[:open_time]
    @close_time = params[:close_time]
    @symbol = params[:symbol]
    @quantity = params[:quantity]
    @pnl = params[:pnl]
    @open_side = params[:open_side]
    @close_side = params[:close_side]
    @open_price = params[:open_price]
    @close_price = params[:close_price]
  end

  def print
    puts "OPEN TIME: #{@open_time}, CLOSE TIME: #{@close_time}, SYMBOL: #{@symbol}, QTY: #{@quantity},"
    puts "PNL Realized: #{@pnl}, OPEN SIDE: #{@open_side}, CLOSE SIDE: #{@close_side}, OPEN PRICE: #{@open_price},"
    puts "CLOSE PRICE: #{@close_price}"
  end
end
