# frozen_string_literal: true

require 'rspec'
require_relative 'copy'

RSpec.describe TradePNL do
  describe '#run' do
    subject(:run) do
      trade_pnl.run(time, symbol, side, price, quantity)
    end

    let(:trade_pnl) { described_class.new('') }

    context 'when no order exists' do
      let(:time) { 1 }
      let(:symbol) { 'OKC' }
      let(:side) { 'B' }
      let(:price) { 49.45 }
      let(:quantity) { 200 }

      it 'submits the order duplicated' do
        run
        expect(trade_pnl.order_list.length).to eq(2)
      end

      it 'does not modify total pnl value' do
        run
        expect(trade_pnl.total_pnl).to eq(0)
      end
    end

    context 'when the first order can be closed and matched' do
      # 29,OKC,S,49.51,300
      let(:time) { 44 }
      let(:symbol) { 'OKC' }
      let(:side) { 'S' }
      let(:price) { 49.52 }
      let(:quantity) { 100 }

      before do
        trade_pnl.order_list << Order.new(trade_pnl.trade_params(1, 'OKC', 'B', 49.45, 200))
        # trade_pnl.order_list << Order.new(trade_pnl.trade_params(1, 'OKC', 'B', 49.45, 200))
      end

      it 'closes the existent order' do
        run
        expect(trade_pnl.closed_trades.length).to eq(1)
      end

      it 'modifies total pnl value' do
        run
        expect(trade_pnl.total_pnl).not_to eq(0)
      end

      it 'does not remove the order from list' do
        expect(trade_pnl.order_list.length).to eq(1)
        run
        expect(trade_pnl.order_list.length).to eq(1)
      end
    end

    context 'when the first order can be closed and it is the only one from list' do
      # 29,OKC,S,49.51,300
      let(:time) { 29 }
      let(:symbol) { 'OKC' }
      let(:side) { 'S' }
      let(:price) { 49.51 }
      let(:quantity) { 300 }

      before do
        trade_pnl.order_list << Order.new(trade_pnl.trade_params(1, 'OKC', 'B', 49.45, 200))
        # trade_pnl.order_list << Order.new(trade_pnl.trade_params(1, 'OKC', 'B', 49.45, 200))
      end

      it 'closes the order' do
        run
        expect(trade_pnl.closed_trades.length).to eq(1)
      end

      it 'does not remove the order from the list' do
        expect(trade_pnl.order_list.length).to eq(1)
        run
        expect(trade_pnl.order_list.length).to eq(1)
      end
    end

    context 'when the first order can be closed and there are more trades that can also be closed' do
      # 29,OKC,S,49.51,300
      let(:time) { 29 }
      let(:symbol) { 'OKC' }
      let(:side) { 'S' }
      let(:price) { 49.51 }
      let(:quantity) { 300 }

      before do
        trade_pnl.order_list << Order.new(trade_pnl.trade_params(1, 'OKC', 'B', 49.45, 200))
        # 43,OKC,S,49.52,100
        trade_pnl.order_list << Order.new(trade_pnl.trade_params(2, 'OKC', 'B', 49.52, 100))
      end

      it 'closes all the trades' do
        run
        expect(trade_pnl.closed_trades.length).to eq(2)
      end

      it 'removes the previous trades from list' do
        expect(trade_pnl.order_list.length).to eq(2)
        run
        expect(trade_pnl.order_list.length).to eq(1)
        expect(trade_pnl.order_list.first.time).to eq time
      end
    end
  end
end
