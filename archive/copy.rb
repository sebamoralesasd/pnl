# frozen_string_literal: true

require 'csv'
require 'pry'

# A single order
class Order
  attr_accessor :time, :symbol, :side, :price, :quantity

  def initialize(params)
    @time = params[:time]
    @symbol = params[:symbol]
    @side = params[:side]
    @price = params[:price]
    @quantity = params[:quantity]
  end

  def print
    "#{@time},#{@symbol},#{@side},#{@price},#{@quantity},"
  end
end

class ClosedOrder
  def initialize(params)
    @open_time = params[:open_time]
    @close_time = params[:close_time]
    @symbol = params[:symbol]
    @quantity = params[:quantity]
    @pnl = params[:pnl]
    @open_side = params[:open_side]
    @close_side = params[:close_side]
    @open_price = params[:open_price]
    @close_price = params[:close_price]
  end

  def print
    "#{@open_time},#{@close_time},#{@symbol},#{@quantity},#{@pnl},#{@open_side},#{@close_side},#{@open_price},#{@close_price},"
  end
end

class TradePNL
  attr_accessor :order_list, :closed_trades, :total_pnl

  def initialize(csv_file)
    @csv = CSV.read(csv_file).drop(1) unless csv_file == ''
    @order_list = []
    @closed_trades = []
    @total_pnl = 0
  end

  def trade_params(time, symbol, side, price, quantity)
    {
      time: time,
      symbol: symbol,
      side: side,
      price: price,
      quantity: quantity
    }
  end

  def closed_trade_params(open_time, close_time, symbol, quantity, pnl, open_side, close_side, open_price, close_price)
    {
      open_time: open_time,
      close_time: close_time,
      symbol: symbol,
      quantity: quantity,
      pnl: pnl,
      open_side: open_side,
      close_side: close_side,
      open_price: open_price,
      close_price: close_price
    }
  end

  def can_close_trade?(order, symbol, side)
    (order.symbol == symbol) && (order.side != side)
  end

  def matched?(order, quantity)
    order.quantity >= quantity
  end

  def print_closed_trades
    puts('OPEN_TIME,CLOSE_TIME,SYMBOL,QUANTITY,PNL,OPEN_SIDE,CLOSE_SIDE,OPEN_PRICE,CLOSE_PRICE')
    @closed_trades.each do |tr|
      puts tr.print
    end
    nil
  end

  def print_open_trades
    puts('TIME,SYMBOL,SIDE,PRICE,QUANTITY')
    @order_list.each do |tr|
      puts tr.print
    end
    nil
  end

  def print_trades
    puts 'Open trades'
    puts print_open_trades
    puts 'Close trades'
    puts print_closed_trades
  end

  def fetch_and_run
    @csv.each do |row|
      time = row[0].to_i
      symbol = row[1]
      side = row[2]
      price = row[3].to_f
      quantity = row[4].to_f
      run(time, symbol, side, price, quantity)
      print_trades
    end

    print_closed_trades

    @total_pnl
  end

  def pnl(trade_price, price, quantity)
    ((trade_price - price) * quantity).abs
  end

  def run(time, symbol, side, price, quantity)
    @order_list.append(Order.new(trade_params(time, symbol, side, price, quantity))) if @order_list == []

    first_open_trade = @order_list.first

    # Matching symbols, and reverse orders
    unless can_close_trade?(first_open_trade, symbol, side)
      @order_list.append(Order.new(trade_params(time, symbol, side, price, quantity)))
      return
    end

    unless matched?(first_open_trade, quantity)
      # CLOSING ORDER IS BIGGER THAN OPENING ORDER
      pnl = pnl(first_open_trade.price, price, first_open_trade.quantity)
      @total_pnl += pnl
      quantity -= first_open_trade.quantity

      @closed_trades.append(ClosedOrder.new(closed_trade_params(first_open_trade.time, time, symbol, first_open_trade.quantity, pnl,
                                                                first_open_trade.side, side, first_open_trade.price, price)))
      # Se elimina el primer order de la lista
      @order_list.shift

      @order_list.each do |open_trade|
        next unless open_trade.symbol == symbol

        if matched?(open_trade, quantity)
          # CLOSING TRADE IS FINALLY MATCHED
          pnl = pnl(open_trade.price, price, quantity)
          @total_pnl += pnl
          open_trade.quantity -= quantity

          # Adding result to the closed_order list
          @closed_trades.append(ClosedOrder.new(closed_trade_params(open_trade.time, time, symbol, quantity, pnl,
                                                                    open_trade.side, side, open_trade.price, price)))
          if open_trade.quantity.zero?
            @order_list.delete(open_trade)
            break
          end
        else # CLOSING TRADE REMAINS UNMATCHED
          pnl = pnl(open_trade.price, price, open_trade.quantity)
          @total_pnl += pnl
          quantity -= open_trade.quantity
          @order_list.delete(open_trade)
        end
      end
      # CLOSING ORDER IS BIGGER THAN ENTIRE INVENTORY
      @order_list.append(Order.new(trade_params(time, symbol, side, price, quantity))) if quantity.positive?
      return
    end

    # CLOSING ORDER IS CLOSED AND MATCHED
    pnl = pnl(first_open_trade.price, price, quantity)
    @total_pnl += pnl
    first_open_trade.quantity -= quantity

    # Adding result to closed_order list
    @closed_trades.append(ClosedOrder.new(closed_trade_params(first_open_trade.time, time, symbol, quantity, pnl,
                                                              first_open_trade.side, side, first_open_trade.price, price)))

    @order_list.shift if first_open_trade.quantity.zero?
  end
end

def main
  raise 'Falta CSV' if ARGV.length != 1

  csv_file = "./#{ARGV.first}"

  runner = TradePNL.new(csv_file)

  puts(runner.fetch_and_run)
end

main
